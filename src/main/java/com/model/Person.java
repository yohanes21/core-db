/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.model;

/**
 *
 * @author yohanez
 */
public class Person {
    private String name;
    private String alamat;
    
    public Person(){
        
    }
    
    public String getName(){
        return name;
    }
    public void setName(String name){
        this.name=name;
    }
    public String getAlamat(){
        return alamat;
    }
    public void setAlamat(String alamat){
        this.alamat=alamat;
    }
    
    @Override //overriding method
    public String toString(){
        return "Person{name:"+name+",alamat:"
                + alamat +"}";
    }
}

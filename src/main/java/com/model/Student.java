/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.model;

/**
 *
 * @author yohanez
 */
public class Student extends Person{
    private String noInduk;
    private String sekolah;
    
    public Student(){
        super();
    }
    public String getNoInduk(){
        return noInduk;
    }
    public void setNoInduk(String noInduk){
        this.noInduk=noInduk;
    }
    public String getSekolah(){
        return sekolah;
    }
    public void setSeskolah(String sekolah){
        this.sekolah=sekolah;
    }
    @Override
    public String toString(){
        return "Student{name:"+getName()
                +",alamat:"+getAlamat()
                +",noInduk:"+noInduk
                +",sekolah:"+sekolah
                +"}";
                
    }
    
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.model;

/**
 *
 * @author yohanez
 */
public class Category {

    private int id;
    private String name;

    public Category(){ //default construktor
     
    }
    public Category(String name) {
        this.name=name;
    }
    
    public Category(int id, String name){//constructor parmeter
        this.id=id;
        this.name=name;
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

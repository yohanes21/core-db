/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.modeldao;

import com.model.Category;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author yohanez
 */
public interface CategoryDAO {

    int save(Category category) throws SQLException;

    int update(int id, Category category) throws SQLException;

    int delete(int id) throws SQLException;

    Category findById(int id) throws SQLException;
    List<Category> findAll () throws SQLException;

    List<Category> findByName(String name) throws SQLException;
}

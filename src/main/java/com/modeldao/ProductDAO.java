/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.modeldao;

import com.model.Product;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author yohanez
 */
public interface ProductDAO {

    int save(Product product) throws SQLException;

    int update(int id, Product product) throws SQLException;

    int delete(int id) throws SQLException;

    Product findById(int id) throws SQLException;

    List<Product> findAll() throws SQLException;

    List<Product> findByName(String name) throws SQLException;
}

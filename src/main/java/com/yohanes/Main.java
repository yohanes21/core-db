package com.yohanes;

import com.abstrac.Cow;
import com.abstrac.Human;
import com.abstrac.LivingThing;
import com.model.Category;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.TreeSet;

/**
 * Hello world!
 *
 */
public class Main 
{
    public static void main( String[] args ){
        Object [] datas = {"2","a","1","4","a","7","d","0","6","7"};
        Integer [] datasInteger={2,0,1,3,7,0,6,7};
        Collection list=new ArrayList();
        addCollectionValues(list, datas);
        showCollection(list);
        
        Collection set=new HashSet();
        addCollectionValues(set, datas);
        showCollection(set);
        
        Collection treeSet=new TreeSet();
        addCollectionValues(treeSet, datasInteger);
        showCollection(treeSet);
        
        Map map = new HashMap();
        map.put("Satu",1);
        map.put(2,"Dua");
        map.put(3,"Tiga");
        map.put(4,"Empat");
        System.out.println("map");
        System.out.println(map.get(3));
    }
    public static void addCollectionValues(Collection collection, Object [] datas){
      for (int i=0;i<datas.length;i++){
          collection.add(datas[i]);
      }  
    }
//        LivingThing human =new Human();
//        LivingThing cow= new Cow();
//        
//        describe (human);
//        describe(cow);
//        describe(new LivingThing(){
//            @Override
//            public void walk(){
//                System.out.println("Kanggoro walk by jump");
//            }
//        });
    public static void showCollection(Collection collection){
        System.out.println(collection.getClass());
        for (Object o:collection){
            System.out.println(o);
           }
        }
           
    public static void describe(LivingThing livingThing){
        System.out.println("\n"+livingThing.getClass());
        livingThing.breath();
        livingThing.eat();
        livingThing.walk();
    }
    
    public static void showCategory (Category category){
        System.out.println("category Id : "+category.getId());
        System.out.println("Category Name : "+category.getName());
    }
    public static void overload(){
        
    }
}
